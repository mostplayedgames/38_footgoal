﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class WindManager : MonoBehaviour {

	private static WindManager s_instance;
	public static WindManager Instance()
	{
		return s_instance;
	}

	private static float RIGHT_ROT = 10;
	private static float LEFT_ROT  = 170;

	public GameObject m_windZone;
	public GameObject m_windParticles;
	public GameObject m_leafParticles;

	public GameObject m_rightPos;
	public GameObject m_leftPos;

	public GameObject m_directionIcon;
	public Text m_windText;

	void Awake () 
	{
		s_instance = this;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	public void SetPos(Vector3 p_cameraPos)
	{
		this.transform.localPosition = new Vector3 (p_cameraPos.x, this.transform.position.y, this.transform.position.z);
	}

//	public void activateWind(bool p_status)
//	{
//		if (p_status ) 
//		{
//			m_windParticles.gameObject.SetActive (false);
//			m_leafParticles.gameObject.SetActive (false);
//			m_windText.gameObject.SetActive (false);
//			m_windZone.gameObject.SetActive (false);
//			m_directionIcon.gameObject.SetActive (false);
//
//			this.GetComponent<AudioSource> ().volume = 0;
//		} 
//		else 
//		{
//			if (m_windParticles.gameObject.activeSelf) 
//			{
//				return;
//			}
//
//			m_windParticles.gameObject.SetActive (true);
//			m_leafParticles.gameObject.SetActive (true);
//			m_windText.gameObject.SetActive (true);
//			m_windZone.gameObject.SetActive (true);
//			m_directionIcon.gameObject.SetActive (true);
//		}
//	}


//	public void SetUpLevel(WindDirection p_direction, float p_intensity)
//	{
//		//this.transform.position = new Vector3 (p_cameraPos.x, this.transform.position.y, this.transform.position.z);
//		float intensity  = GetIntensity (p_intensity);
//		switch (p_direction) 
//		{
//			case WindDirection.LEFT:
//			{
//				m_windParticles.transform.localPosition = m_leftPos.transform.localPosition;
//				m_windParticles.transform.eulerAngles = new Vector3 (LEFT_ROT, -90, 90);
//				m_leafParticles.transform.localPosition = m_leftPos.transform.localPosition;
//				m_leafParticles.transform.eulerAngles = new Vector3 (LEFT_ROT, -90, 90);
//
//				m_directionIcon.transform.eulerAngles =new Vector3(m_directionIcon.transform.eulerAngles.x,
//																	m_directionIcon.transform.eulerAngles.y 
//																	,0);
//				break;
//			}
//			case WindDirection.RIGHT:
//			{
//				m_windParticles.transform.localPosition = m_rightPos.transform.localPosition;
//				m_windParticles.transform.eulerAngles = new Vector3 (RIGHT_ROT, -90, 90);
//				m_leafParticles.transform.localPosition = m_rightPos.transform.localPosition;
//				m_leafParticles.transform.eulerAngles = new Vector3 (RIGHT_ROT, -90, 90);
//				m_directionIcon.transform.eulerAngles = new Vector3(m_directionIcon.transform.eulerAngles.x,
//																		m_directionIcon.transform.eulerAngles.y 
//																		,180);
//				
//				intensity *= -1;
//				break;
//			}
//		}
//
//		float intensityVal = intensity * 0.10f;
//		m_windText.text = "" + Mathf.Abs(intensityVal) + "m";
//		m_windZone.GetComponent<AreaEffector2D> ().forceMagnitude = intensity;
//	}
//
	private float GetIntensity(float p_intensity)
	{
		float retVal = 0;
		retVal = remapValue (p_intensity, 0, 5, 0, 100);
//		switch (p_intensity) 
//		{
//			case 0:
//			{
//				retVal = 0;
//				this.GetComponent<AudioSource> ().volume = 0.0f;
//
//				break;
//			}
//			case 1:
//			{
//				retVal = 10;
//				this.GetComponent<AudioSource> ().volume = 0.2f;
//
//				break;
//			}
//			case 2:
//			{
//				retVal = 30;
//				this.GetComponent<AudioSource> ().volume = 0.5f;
//
//				break;
//			}
//			case 3:
//			{
//				retVal = 50;
//				this.GetComponent<AudioSource> ().volume = 0.7f;
//
//				break;
//			}
//			case 4:
//			{
//				retVal = 40;
//				this.GetComponent<AudioSource> ().volume = 0.9f;
//
//				break;
//			}
//		default:
//			{
//				break;
//			}
//		}

		return retVal;
	}

	private float remapValue(float p_value, float p_from1, float p_to1, float p_from2, float p_to2)
	{
		return (p_value - p_from1) / (p_to1 - p_from1) * (p_to2 - p_from2) + p_from2;
	}
}
