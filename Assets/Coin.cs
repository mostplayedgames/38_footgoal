﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour 
{
	public Vector3 screenPos;

	public void Start()
	{
		Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
		pos.x = Random.Range(0.1f, 0.9f);
		pos.y = Random.Range(0.4f, 0.9f);
		pos.z = 150;
		screenPos = pos;
		transform.position = Camera.main.ViewportToWorldPoint(pos);
	}

	private void OnTriggerEnter2D(Collider2D collision)
	{
		if (collision.tag == "Player")
		{
			FGKickMode.instance.AddCoins(1, true);
		}
	}

	void LateUpdate()
	{
			transform.position = Camera.main.ViewportToWorldPoint(screenPos);
	}
}
