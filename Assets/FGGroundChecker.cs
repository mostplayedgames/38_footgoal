﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGGroundChecker : MonoBehaviour {

	public FGCharacter m_scriptCharacter;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Die") 
		{
			m_scriptCharacter.m_characterGround = 1;
			//Debug.Log ("Trigger Enter Add : " + m_scriptCharacter.m_characterGround);
		}
	//	Debug.Log ("Trigger Enter");
	}

	void OnTriggerExit2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Die") 
		{
			m_scriptCharacter.m_characterGround = 0;
		//	Debug.Log ("Trigger Exit Subtract : " + m_scriptCharacter.m_characterGround);
		}
		//Debug.Log ("Trigger Exit ");
	}

	void OnTriggerStay2D(Collider2D coll)
	{
		if (coll.gameObject.tag == "Die") 
		{
			m_scriptCharacter.m_characterGround = 1;
			//Debug.Log ("Trigger Enter Add : " + m_scriptCharacter.m_characterGround);
		}
		//Debug.Log ("Trigger Enter");
	}
}
