﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

/// <summary>
/// 
/// </summary>
public class DieChecker : MonoBehaviour
{
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "BALL" && !coll.gameObject.GetComponent<SBBall>().m_DidScore)// && coll.gameObject.GetComponent<SBBall>().m_curState != BallState.SCORE) 
		{
			GameScene.instance.Die ();
		}
	}
}
