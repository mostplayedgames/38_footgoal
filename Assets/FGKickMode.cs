﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

using System.Linq;

public enum FGKickModeEnum
{
	IDLE,
	CHARGE,
	SHOOT,
	RESULTS,
	SCORE
}

[SerializeField]
[Serializable]
public class Level
{
	public string name;
	public Vector2 playerPos;
	public Vector2 ballPos;
	public Vector2 goalPos;
	public float goalAngle;
	public float groundAngle;
	public bool isGroundGoal = false;
	public bool isDefaultBall = true;
	public bool isDefaultCamera = true;
	public Vector2 cameraPos;
	public float cameraSize;
	public float goalSpeed = 0;
	public float goalDistance = 0;
	public float goalWait = 0;
	public float goalSpin = 0;
	public Vector2 platform;
	public float platformAngle = 0;

	public Level(string nam, Vector2 playPos, Vector2 bllPos, Vector2 golPos, float goalAngl, float grndAngl, bool groundGoal, bool defaultBall, bool defaultCam, Vector2 camPos, float camSize,
	float golSpd, float golDist, float golWait, float golSpn, Vector2 plat, float platAngle)
	{
		name = nam;
		playerPos = playPos;
		ballPos = bllPos;
		goalPos = golPos;
		groundAngle = grndAngl;
		goalAngle = goalAngl;
		isGroundGoal = groundGoal;
		isDefaultBall = defaultBall;
		isDefaultCamera = defaultCam;
		cameraPos = camPos;
		cameraSize = camSize;
		goalSpeed = golSpd;
		goalDistance = golDist;
		goalWait = golWait;
		goalSpin = golSpn;
		platform = plat;
		platformAngle = platAngle;
	}
}

[SerializeField]
[Serializable]
public class PlayerCharacter
{
	public string id;
	public string name;
	public bool locked;
	public Sprite spriteHead;
	public Sprite spriteBody;
}

[SerializeField]
[Serializable]
public class GameMode
{
	public string id;
	public string name;
}

public class FGKickMode : FDGamesceneInstance 
{
	[Header("Game")]
	public bool TEST_MODE = false;

	public static FGKickMode instance;
	public FGKickModeEnum m_eState;

	public Text m_textTries;

	public FGCharacter m_scriptCharacter;
	public GameObject m_fgCharacter;
	public SpriteRenderer m_spriteCharacterHead;
	public SpriteRenderer m_spriteCharacterBody;

	public FGGoal m_scriptGoal;

	public GameObject m_objectBall;
	public GameObject m_objectGround;
	public GameObject m_objectGoalBottom;
	public GameObject m_objectPlatform;

	public GameObject m_objectTitle;
	public GameObject m_objectMainUI;

	public Vector3 m_defaultCameraPos;
	public float m_defaultCameraOrthographicSize = 20;

	public Transform m_leftBound;
	public Transform m_rightBound;

	[SerializeField]
	public List<Level> m_listLevel;
	private int currCharacter;

	[Header("UI")]
	public Text scoreText;
	public Text scoreLabel;

	[Header("Shop")]
	public int coins;
	public int coinsNeeded;
	public GameObject shopTitleText;

	public Image shopHead;
	public Image shopBody;

	public GameObject currentCoin;
	public GameObject coinObject;
	public float coinSpawnTimer;
	public float coinSpawnTime = 30;
	public bool coinSpawned = false;

	[SerializeField]
	public List<PlayerCharacter> playerCharacters;
	public List<string> lockedList;

	public GameObject shopUI;
	public bool shopShown;
	public Text shopName;
	public Text shopDesc;
	public Text textPrice;
	public int numPurchased;
	public List<int> listPrices;
	public Transform shopItemUITemplate;
	public Transform shopListUI;
	public List<ShopObject> shopObjects;
	public GameObject shopNotif;

	public Transform coinBar;
	public Text coinText;
	public Text coinsNeededText;

	public Animator flash;

	public Button retryButton;

	[Header("Ads")]
	public int numRetries = 0;
	public Text textRetriesLeft;
	public float adTime = 60;
	public float adTimer;
	public bool adReady;

	[Header("Modes")]
	[SerializeField]
	public List<GameMode> gameModes;
	public int currentMode = 0;
	public Text modeText;
	public GameObject m_moreLevelsToCome;

	public int score;
	public bool isHighScore = false;

	[Header("Head Mode")]
	public float distance;
	public float maxDistance;

	[Header("Quick Mode")]
	public Image clock;
	public Text clockText;
	public bool hasStarted = false;
	public float timeLeft;
	public float maxTime = 30;
	public AudioClip scoreSound;

	public void PrevMode()
	{
		currentMode--;
		if (currentMode < 0) currentMode = gameModes.Count - 1;
		ChangeMode(currentMode);
	}

	public void NextMode()
	{
		currentMode++;
		if (currentMode >= gameModes.Count) currentMode = 0;
		ChangeMode(currentMode);
	}

	void ChangeMode(int index)
	{
		currentMode = index;
		modeText.text = gameModes[currentMode].name + " Mode";

		if (adReady || numRetries >= 3)
		{
			ShowAd();
			numRetries = 0;
			adReady = false;

			adTimer = Time.time + adTime;

			textRetriesLeft.text = "AD IN 3";
		}

		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);

		UpdateMode();
	}

	public void UpdateMode()
	{
		score = 0;
		isHighScore = false;
		clock.gameObject.SetActive(false);
		timeLeft = 0;
		hasStarted = false;
		retryButton.gameObject.SetActive(false);

		switch (gameModes[currentMode].id)
		{
			case "LEVEL":
				scoreLabel.text = "LEVEL";
				scoreText.text = GameScene.instance.GetLevel().ToString();
				retryButton.gameObject.SetActive(true);
				break;

			case "QUICK":
				scoreLabel.text = "BEST";
				scoreText.text = PlayerPrefs.GetInt("QUICK High Score", 0).ToString();
				
				clock.fillAmount = 1;
				timeLeft = maxTime;
				clockText.text = timeLeft.ToString();
				hasStarted = false;
				break;

			case "HEAD":
				distance = PlayerPrefs.GetFloat("HEIGHT High Score", 0);
				scoreLabel.text = "HEIGHT";
				scoreText.text = distance.ToString("0.#");
			break;
		}

		Gamescene_Setuplevel(true);
	}

	public void LateUpdate()
	{
		if (hasStarted)
		{
			clock.fillAmount = timeLeft / maxTime;
			clockText.text = Mathf.CeilToInt(timeLeft).ToString();
		}

		if (!coinSpawned)
		{
			if (coinSpawnTimer <= Time.time)
			{
				SpawnCoin();
			}
		}
	}

	void Awake () 
	{
//		PlayerPrefs.DeleteAll();
		instance = this;

		m_defaultCameraPos = new Vector3(6.6f, 0, -143);

		ReadCSV();

//		UnlockAll();
		LoadGame();
		SetupShop();

		if (coins >= coinsNeeded) shopNotif.SetActive(true);
		else shopNotif.SetActive(false);
	}

	private void Start()
	{
		UpdateMode();

		coinSpawnTimer = Time.time + coinSpawnTime;
		adTimer = Time.time + adTime;
	}

	public void SetCameraDefault()
	{
		Camera.main.transform.position = m_defaultCameraPos;
		Camera.main.orthographicSize = m_defaultCameraOrthographicSize;
	}

	public void SetCamera(Vector3 newPos, float newSize)
	{
		Camera.main.transform.position = new Vector3(newPos.x, newPos.y, -143);
		Camera.main.orthographicSize = newSize;
	}

	public void HideTitle()
	{
		m_objectTitle.SetActive(false);
		m_objectMainUI.SetActive(true);
	}

	void Update()
	{
	/*	if (true && Input.GetMouseButtonDown (0)) {
			m_scriptCharacter.Kick ();
			m_objectTitle.SetActive (false);
		}
		if (true && Input.GetMouseButtonDown (1)) {
			m_scriptCharacter.KickBack ();
			m_objectTitle.SetActive (false);
		}
		*/

		if (Input.GetMouseButtonDown (0) && !GameScene.instance.IsPointerOverUIObject) {
			HideTitle();
		}

		if (Input.GetKeyDown (KeyCode.D)) 
		{
			m_scriptCharacter.Kick ();
			HideTitle();
		}

		if (Input.GetKeyDown (KeyCode.A)) 
		{
			m_scriptCharacter.KickBack ();
			HideTitle();
		}

		switch (gameModes[currentMode].id)
		{
			case "LEVEL":
				scoreLabel.text = "LEVEL";
				scoreText.text = GameScene.instance.GetLevel().ToString();
			break;

			case "QUICK":
				if (hasStarted)
				{
					timeLeft = Mathf.Max(0, timeLeft - Time.deltaTime);

					if (timeLeft <= 0)
					{
						ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioDie);
						hasStarted = false;
						scoreLabel.text = "FINAL";
						clockText.text = "TIME'S UP!\n<color=orange>Try again?</color>";
						m_objectGoal.SetActive(false);
						retryButton.gameObject.SetActive(true);
					}
				}
			break;

			case "HEAD":
				if (m_objectBall.transform.position.y >= distance)
				{
					distance = m_objectBall.transform.position.y;
					scoreText.text = distance.ToString("0.#");
					PlayerPrefs.SetFloat("HEIGHT High Score", distance);
					isHighScore = true;
					Debug.Log ("Head Distance Up");
				}
				
				if (isHighScore && m_objectBall.GetComponent<Rigidbody2D>().velocity.y < 0)
				{
					Debug.Log ("PostScore HEAD");

					int actualScore = (int) (PlayerPrefs.GetFloat ("HEIGHT High Score") * 10);

					#if UNITY_ANDROID
						ZPlatformCenterMgr.Instance.PostScore(actualScore, ZGameMgr.instance.m_listLeaderboard[currentMode].leaderboardAndroid);
					#else
						ZPlatformCenterMgr.Instance.PostScore (actualScore, ZGameMgr.instance.m_listLeaderboard[currentMode].leaderboardIOS);
					#endif

					isHighScore = false;
				}
				break;
		}

		textRetriesLeft.gameObject.SetActive(ZAdsMgr.Instance.IsInterstitialAvailable());

		if (adTimer <= Time.time)
		{
			AdReady();
		}

		UpdateCoinsUI();
	}

	public void AdReady()
	{
		adReady = true;
		textRetriesLeft.text = "AD READY";
	}

	public override void Gamescene_Score ()
	{
		AddCoins(3);
	}

	public void AddScore(int points=1)
	{
		Debug.Log ("AddScoreFGKick");

		score += points;
		scoreLabel.text = "SCORE";
		scoreText.text = score.ToString();
		
		if (!hasStarted)
		{
			hasStarted = true;
			clock.gameObject.SetActive(true);
		}

		ZAudioMgr.Instance.PlaySFXVolume(scoreSound, 0.5f);
		AddTime(15);
		AddCoins(1);

		int highScore = PlayerPrefs.GetInt (gameModes [currentMode].id + " High Score", 0);
		Debug.Log ("HighScore: " + highScore + "_ Score: " + score);

		if (score > PlayerPrefs.GetInt(gameModes[currentMode].id + " High Score", 0))
		{
			scoreLabel.text = "NEW BEST";
			PlayerPrefs.SetInt(gameModes[currentMode].id + " High Score", score);
			Debug.Log ("PostScore AddScore");
			#if UNITY_ANDROID
				ZPlatformCenterMgr.Instance.PostScore(GameScene.instance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[currentMode].leaderboardAndroid);
			#else
				ZPlatformCenterMgr.Instance.PostScore (GameScene.instance.GetBestScore(), ZGameMgr.instance.m_listLeaderboard[currentMode].leaderboardIOS);
			#endif
		}

		Gamescene_Setuplevel(true);
	}

	public void AddTime(float amount)
	{
		if (hasStarted) timeLeft = Mathf.Min(maxTime, timeLeft + 30);
	}

	public GameObject m_objectGoal;

	public void LoadGame()
	{
		if (!PlayerPrefs.HasKey("Removeads"))
		{
			PlayerPrefs.SetInt("Removeads", 0);
		}

		coins = PlayerPrefs.GetInt("Coins", 0);

		numPurchased = PlayerPrefs.GetInt("Items Purchased", 0);

		ChangeCharacter(PlayerPrefs.GetInt("Current Character", 0));

		if (!PlayerPrefs.HasKey(playerCharacters[0].id + " Locked"))
		{
			PlayerPrefs.SetInt(playerCharacters[0].id + " Locked", 0);
		}

		int index = 0;
		foreach (PlayerCharacter s in playerCharacters)
		{
			s.locked = (PlayerPrefs.GetInt(s.id + " Locked", 1) == 1 ? true : false);
			if (s.id == "DEFAULT") s.locked = false;

			if (s.locked)
			{
				lockedList.Add(s.id);
			}
			
			index++;
		}

		if (numPurchased < listPrices.Count) coinsNeeded = listPrices[numPurchased];
		else coinsNeeded = listPrices[listPrices.Count - 1];

		textPrice.text = coinsNeeded.ToString();
	}

	void SpawnLevel(int newLevel)
	{
		if (newLevel >= 1)
		{
			m_objectGoal.SetActive(true);
			if (m_listLevel[newLevel].playerPos != Vector2.zero) m_scriptCharacter.transform.localPosition = m_listLevel[newLevel].playerPos;
			else m_scriptCharacter.transform.localPosition = Vector3.zero;

			m_scriptCharacter.transform.localEulerAngles = Vector3.zero;

			if (m_listLevel[newLevel].ballPos != Vector2.zero) m_objectBall.transform.localPosition = m_listLevel[newLevel].ballPos;
			else m_objectBall.transform.localPosition = new Vector3(6.7f, 0.67f, 0);

			m_objectBall.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			m_objectBall.GetComponent<Rigidbody2D>().angularVelocity = 0f;

			m_eState = FGKickModeEnum.IDLE;

			m_scriptCharacter.Reset();

			// the part above this used to be in the setup script

			m_objectGoal.SetActive(true);

			float xPosition = m_listLevel[newLevel].goalPos.x;
			float yPosition = m_listLevel[newLevel].goalPos.y;
			float angle = m_listLevel[newLevel].goalAngle;

			m_objectGoal.transform.localPosition = new Vector3(xPosition, yPosition, 0);
			m_objectGoal.transform.localEulerAngles = new Vector3(0, 0, angle);
			m_scriptGoal.SetGoal(m_listLevel[newLevel].goalDistance, m_listLevel[newLevel].goalSpeed, m_listLevel[newLevel].goalSpin);

			float groundAngle = m_listLevel[newLevel].groundAngle;
			bool goalHalfGoal = m_listLevel[newLevel].isGroundGoal;

			m_objectGround.transform.localPosition = new Vector3(0, -1.52f, 0);
			m_objectGround.transform.localEulerAngles = new Vector3(0, 0, groundAngle);

			if (!goalHalfGoal) m_objectGoalBottom.SetActive(true);
			else m_objectGoalBottom.SetActive(false);

			float platformXPosition = m_listLevel[GameScene.instance.GetLevel()].platform.x;
			float platformYPosition = m_listLevel[GameScene.instance.GetLevel()].platform.y;
			float platformAngle = m_listLevel[newLevel].platformAngle;

			if (platformXPosition == 0 && platformXPosition == 0) m_objectPlatform.SetActive(false);
			else m_objectPlatform.SetActive(true);

			m_objectPlatform.transform.localPosition = new Vector3(platformXPosition, platformYPosition, 0);
			m_objectPlatform.transform.localEulerAngles = new Vector3(0, 0, platformAngle);

			if (!m_listLevel[newLevel].isDefaultCamera) SetCamera(m_listLevel[newLevel].cameraPos, m_listLevel[newLevel].cameraSize);
			else SetCameraDefault();
		}
		else if (newLevel == 0)
		{ 
			m_objectGoal.SetActive(true);

			float xPosition = UnityEngine.Random.Range(0, 15);
			float yPosition = UnityEngine.Random.Range(0, 10);
			float angle = (xPosition >= 7.5f ? UnityEngine.Random.Range(-90, 90): UnityEngine.Random.Range(90, 270));

			m_objectGoal.transform.localPosition = new Vector3(xPosition, yPosition, 0);
			m_objectGoal.transform.localEulerAngles = new Vector3(0, 0, angle);
			m_scriptGoal.SetGoal(0, 0, 0);

			float groundAngle = UnityEngine.Random.Range(-3, 3);

			m_objectGround.transform.localPosition = new Vector3(0, -1.52f, 0);
			m_objectGround.transform.localEulerAngles = new Vector3(0, 0, groundAngle);

			m_objectGoalBottom.SetActive(true);

			m_objectPlatform.SetActive(false);
			
			SetCameraDefault();
		}
		else
		{
			m_objectGoal.SetActive(true);
			m_scriptCharacter.transform.localPosition = Vector3.zero;

			m_scriptCharacter.transform.localEulerAngles = Vector3.zero;

			m_objectBall.transform.localPosition = new Vector3(14.5f, 0.67f, 0);

			m_objectBall.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			m_objectBall.GetComponent<Rigidbody2D>().angularVelocity = 0f;

			m_eState = FGKickModeEnum.IDLE;

			m_scriptCharacter.Reset();

			m_objectGoal.SetActive(true);


			float xPosition = 7.5f;
			float yPosition = 0;
			float angle = -90;

			m_objectGoal.transform.localPosition = new Vector3(xPosition, yPosition, 0);
			m_objectGoal.transform.localEulerAngles = new Vector3(0, 0, angle);
			m_scriptGoal.SetGoal(0, 0, 0);

			m_objectGround.transform.localPosition = new Vector3(0, -1.52f, 0);
			m_objectGround.transform.localEulerAngles = new Vector3(0, 0, 0);

			m_objectGoalBottom.SetActive(true);
			m_objectPlatform.SetActive(false);

			SetCameraDefault();
		}
	}

	public override void Gamescene_Die ()
	{

	}

	public override void Gamescene_Setuplevel(bool hardreset)
	{

		switch (gameModes[currentMode].id)
		{

			case "LEVEL":
				if (GameScene.instance.GetLevel() < m_listLevel.Count)
				{
					SpawnLevel(GameScene.instance.GetLevel());
				}
				else
				{
					m_objectGoal.SetActive(false);
					m_moreLevelsToCome.SetActive(true);
				}
				break;

			case "HEAD":
				m_scriptCharacter.transform.localPosition = Vector3.zero;

				m_scriptCharacter.transform.localEulerAngles = Vector3.zero;
				m_objectBall.transform.localPosition = new Vector3(6.7f, 0.67f, 0);

				m_objectBall.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
				m_objectBall.GetComponent<Rigidbody2D>().angularVelocity = 0f;

				m_eState = FGKickModeEnum.IDLE;

				m_scriptCharacter.Reset();

				SpawnLevel(-1);
				m_objectGoal.SetActive(false);
				break;

			case "QUICK":
				// load random
				m_objectGoal.SetActive(true);
				if (score == 0) SpawnLevel(-1);
				else SpawnLevel(0);
			break;
		}

		if (ZRewardsMgr.instance.WillShowRewardSuccess())
		{
			ZRewardsMgr.instance.ShowRewardSuccess();
		}
	}


	public override void Gamescene_Unloadlevel() 
	{
		
	}

	public TextAsset csvFile;

	public void ReadCSV()
	{
		string[,] grid = SplitCsvGrid(csvFile.text);
		// Debug.Log("size = " + (1 + grid.GetUpperBound(0)) + "," + (1 + grid.GetUpperBound(1)));

		DebugOutputGrid(grid);
	}

	// outputs the content of a 2D array, useful for checking the importer
	static public void DebugOutputGrid(string[,] grid)
	{
		string textOutput = "";
		for (int y = 0; y < grid.GetUpperBound(1); y++)
		{
			for (int x = 0; x < grid.GetUpperBound(0); x++)
			{

				textOutput += grid[x, y];
				textOutput += "|";
			}
			textOutput += "\n";
		}
		Debug.Log(textOutput);
	}

	// splits a CSV file into a 2D string array
	static public string[,] SplitCsvGrid(string csvText)
	{
		string[] lines = csvText.Split("\n"[0]);

		// finds the max width of row
		int width = 0;
		for (int i = 0; i < lines.Length; i++)
		{
			string[] row = SplitCsvLine(lines[i]);
			width = Mathf.Max(width, row.Length);
		}

		// creates new 2D string grid to output to
		string[,] outputGrid = new string[width, lines.Length + 1];
		for (int y = 1; y < lines.Length - 1; y++)
		{
			string[] row = SplitCsvLine(lines[y]);

			Level newLevel =
			new Level("Level " + y,
			new Vector2(float.Parse(row[1]), float.Parse(row[2])),
			new Vector2(float.Parse(row[3]), float.Parse(row[4])),
			new Vector2(float.Parse(row[5]), float.Parse(row[6])),
			float.Parse(row[7]),
			float.Parse(row[8]),
			(row[9] == "1" ? true : false),
			(row[10] == "1" ? true : false),
			(row[11] == "1" ? true : false), 
			new Vector2(float.Parse(row[12]), float.Parse(row[13])),
			float.Parse(row[14]),
			float.Parse(row[15]),
			float.Parse(row[16]),
			float.Parse(row[17]),
			float.Parse(row[18]),
			new Vector2(float.Parse(row[19]), float.Parse(row[20])),
			float.Parse(row[21]));

			FGKickMode.instance.m_listLevel.Add(newLevel);
			
			/*
			for (int x = 0; x < row.Length; x++)
			{
				outputGrid[x, y] = row[x];
			}
			*/
		}

		return outputGrid;
	}

	// splits a CSV row 
	static public string[] SplitCsvLine(string line)
	{
		return (from System.Text.RegularExpressions.Match m in System.Text.RegularExpressions.Regex.Matches(line,
																											@"(((?<x>(?=[,\r\n]+))|""(?<x>([^""]|"""")+)""|(?<x>[^,\r\n]+)),?)",
																											System.Text.RegularExpressions.RegexOptions.ExplicitCapture)
				select m.Groups[1].Value).ToArray();
	}


	public void ShowShop()
	{
		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);
		shopUI.SetActive(shopShown);
		shopTitleText.SetActive(shopShown);
		modeText.gameObject.SetActive(!shopShown);

		shopShown = !shopShown;
	}

/*	public void ShowShop(bool forced = false)
	{
		shopUI.SetActive(shopShown);
		shopShown = !shopShown;
	}
	*/
	public void SetupShop()
	{
		int index = 0;

		foreach (PlayerCharacter s in playerCharacters)
		{
			Transform a = Instantiate(shopItemUITemplate, shopListUI) as Transform;
			a.localScale = Vector3.one;
			a.GetComponent<ShopObject>().MakeEntry(s.id, index, s.spriteHead, s.locked, s.name, (s.id == playerCharacters[currCharacter].id ? true : false));
			shopObjects.Add(a.GetComponent<ShopObject>());

			index++;
		}
	}

	public bool AttemptPurchaseItem()
	{
		if (coins >= coinsNeeded || TEST_MODE)
		{
			// Flash();
			return true;
		}
		else return false;
	}

	public void UnlockRandom()
	{
		if (AttemptPurchaseItem())
		{
			if (lockedList.Count > 0)
			{
				string id;
				id = lockedList[UnityEngine.Random.Range(0, lockedList.Count)];

				Debug.Log("Unlocking " + id);
				lockedList.Remove(id);

				UnlockItem(id);
			}
		}
	}

	public bool UnlockItem(string id)
	{
		if (AttemptPurchaseItem())
		{
			int index = 0;
			foreach (PlayerCharacter s in playerCharacters)
			{
				if (s.id == id)
				{
					coins -= coinsNeeded;
					PlayerPrefs.SetInt("Coins", coins);

					numPurchased++;
					PlayerPrefs.SetInt("Items Purchased", numPurchased);

					PlayerPrefs.SetInt(s.id + " Locked", 0);
					s.locked = false;

					Flash.instance.ShowFlash(index);
					ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioSpecialScore);

					// Flash();
					// shopObjects[index].UpdateUnlocked();
					// ChangeCharacter(index);
					// UpdateShopSelected();

					if (numPurchased < listPrices.Count) coinsNeeded = listPrices[numPurchased];
					else coinsNeeded = listPrices[listPrices.Count - 1];
					textPrice.text = coinsNeeded.ToString();

					if (coins >= coinsNeeded) shopNotif.SetActive(true);
					else shopNotif.SetActive(false);

					return true;
				}

				index++;
			}
		}
		return false;
	}

	public void UpdateShop(int index)
	{
		shopObjects[index].UpdateUnlocked();
		ChangeCharacter(index);
		UpdateShopSelected();
	}

	public void UnlockAll()
	{
		numPurchased = -1;

		foreach (PlayerCharacter s in playerCharacters)
		{
			numPurchased++;
			PlayerPrefs.SetInt(s.id + " Locked", 0);

			if (numPurchased < listPrices.Count) coinsNeeded = listPrices[numPurchased];
			else coinsNeeded = listPrices[listPrices.Count - 1];
			textPrice.text = coinsNeeded.ToString();
		}

		PlayerPrefs.SetInt("Items Purchased", numPurchased);
	}

	public void LockAll()
	{
		numPurchased = 0;
		foreach (PlayerCharacter s in playerCharacters)
		{
			PlayerPrefs.SetInt(s.id + " Locked", 1);
		}

		PlayerPrefs.SetInt("Items Purchased", numPurchased);

		ChangeCharacter(0);
	}

	public void Retry()
	{
		numRetries++;

		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);

		if (adReady || numRetries >= 3)
		{
			ShowAd();
			numRetries = 0;
			adReady = false;

			adTimer = Time.time + adTime;
		}

		textRetriesLeft.text = "AD IN " + (3 - numRetries).ToString();

		UpdateMode();
	}

	public void ShowAd()
	{
		ZAdsMgr.Instance.ShowInsterstitial();
	}

	public void ChangeCharacter(string id)
	{
		int index = 0;

		foreach (PlayerCharacter c in playerCharacters)
		{
			if (id == c.id)
			{
				ChangeCharacter(index);
				
				return;
			}
			index++;
		}

	}

	public void ChangeCharacter(int characterIndex)
	{
		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);

		currCharacter = characterIndex;
		PlayerCharacter newChar = playerCharacters[characterIndex];

		m_spriteCharacterBody.sprite = newChar.spriteBody;
		m_spriteCharacterHead.sprite = newChar.spriteHead;

		shopHead.sprite = newChar.spriteHead;
		shopBody.sprite = newChar.spriteBody;

		PlayerPrefs.SetInt("Current Character", characterIndex);

		shopName.text = playerCharacters[characterIndex].name;

		UpdateShopSelected();
	}

	void UpdateShopSelected()
	{
		foreach (ShopObject so in shopObjects)
		{
			// Current Character
			if (so.id == playerCharacters[currCharacter].id)
			{
				so.UpdateSelected(true, true);
			}
			else
			{
				so.UpdateSelected(playerCharacters[so.index].locked, false);
			}

		}
	}

	public void AddCoins(int coinsAdded = 0, bool wasCoin = false)
	{
		coins += coinsAdded;
		PlayerPrefs.SetInt("Coins", coins);

		if (coins >= coinsNeeded) shopNotif.SetActive(true);
		else shopNotif.SetActive(false);

		if (wasCoin)
		{
			ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioCoin);
			Destroy(currentCoin);
			currentCoin = null;
			coinSpawnTimer = Time.time + coinSpawnTime;
			coinSpawned = false;
		}
	}

	public void SpawnCoin()
	{
		if (!coinSpawned)
		{
			coinSpawned = true;
			currentCoin = Instantiate(coinObject);
		}
	}

	public void UpdateCoinsUI()
	{
		coinText.text = coins.ToString() + " / " + coinsNeeded.ToString();
		coinBar.localScale = new Vector3(Mathf.Clamp01(((float) coins) / ((float) coinsNeeded)), 1, 1.0f);
	}

	public void OpenLeaderboard()
	{
		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);
		#if UNITY_ANDROID
				ZPlatformCenterMgr.Instance.ShowLeaderboardUI(ZGameMgr.instance.m_listLeaderboard[currentMode].leaderboardAndroid);
		#else
				ZPlatformCenterMgr.Instance.ShowLeaderboardUI();
		#endif 

	}
}