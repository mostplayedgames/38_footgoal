﻿using UnityEngine;
using System.Collections;

public class HomeScene : ZGameScene {

	public static HomeScene instance;

	void Awake()
	{
		instance = this;
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") {
			ZGameMgr.instance.ShowScene ("GameScene");
		}
		else if (button.name == "btn_shop") {
			ZGameMgr.instance.ShowScene ("ShopScene");
		}
	}

	public override void OnButtonDown(ZButton button)
	{
	}
}
