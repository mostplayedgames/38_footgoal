using UnityEngine;
using System.Collections;

public class MNP 
{	
	public static void ShowPreloader(string title, string message) 
	{
		#if UNITY_IOS 
		MNIOSNative.ShowPreloader();
		#endif
	}
	
	public static void HidePreloader() 
	{
		#if UNITY_IOS 
		MNIOSNative.HidePreloader();
		#endif
	}
}

