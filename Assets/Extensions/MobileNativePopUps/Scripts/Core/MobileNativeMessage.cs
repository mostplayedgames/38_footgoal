using UnityEngine;
using System;
using System.Collections;

public class MobileNativeMessage  {

	public Action OnComplete = delegate {};
	

	public MobileNativeMessage(string title, string message) {
		init(title, message, "Ok");
	}
	
	public MobileNativeMessage(string title, string message, string ok) {
		init(title, message, ok);
	}
	
	
	private void init(string title, string message, string ok) 
	{	
		#if UNITY_IOS
		MNIOSMessage msg  = MNIOSMessage.Create(title, message, ok);
		msg.OnComplete += OnCompleteListener;
		#endif
	}
	
	private void OnCompleteListener(MNDialogResult res) {
		OnComplete();
	}
}

