﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

// Casino Shop is an iteration of the Shop Scene 
public class CasinoShop : ZGameScene 
{

	public static CasinoShop instance;

	// +EDIT JOSH 20170525
	// casino camera is a special camera that renders the bottles unlocked in a different
	// layer so that the bottle would appear above everything in the screen.
	public GameObject m_casinoCam;
	public GameObject m_casinoButtons;
	public GameObject m_casinoBottles;
	public GameObject m_particleSuccess;
	public GameObject m_objectUnlockButton;
	public GameObject m_objectStopButton;

	public Color m_colorNotSelected;
	public Color m_colorSelected;

	public AudioClip m_audioButton;
	public AudioClip m_audioSuccess;
	public AudioClip m_audioOvation;
	public AudioClip m_audioIterate;

	public Text m_textPrice;

	public List<Button> m_listOfButtons;
	public List<GameObject> m_objectBottles;
	public List<GameObject> m_listOfBottlesToIterate;
	public List<Button> m_listOfButtonsToIterate;

	public int m_currentIndex;
	public int m_itemToPurchase;
	private int TARGET_INDEX;
	private int m_iterator;
	private float m_speed;

	void Awake()
	{
		instance = this;
		TARGET_INDEX = 0;
		m_speed = 0.1f;
	}

	void Update()
	{
		if (Input.GetKeyDown (KeyCode.W)) 
		{
			PlayerPrefs.DeleteAll ();
		}
		if (Input.GetKeyDown (KeyCode.A)) 
		{
			ZRewardsMgr.instance.GetTimeDifference ();
		} 
	}

	public void Back()
	{
		this.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);

		m_listOfBottlesToIterate.Clear ();
		m_listOfButtonsToIterate.Clear ();
		GameScene.instance.m_eState = GAME_STATE.IDLE;
	}

	public void SetupScene(bool isNormal = true, bool p_isInitialOpen = false)
	{
		if (!p_isInitialOpen) 
		{
			for (int x = 0; x < m_objectBottles.Count; x++) 
			{
				if (PlayerPrefs.GetInt ("birdbought" + x) > 0) 
				{
					m_objectBottles [x].GetComponent<Renderer> ().material.color = Color.white;
					m_objectBottles [x].GetComponent<ZRotator> ().enabled = true;
				} 
				else 
				{
					m_objectBottles [x].GetComponent<Renderer> ().material.color = Color.black;
					m_objectBottles [x].GetComponent<ZRotator> ().enabled = false;
				}

				m_listOfBottlesToIterate.Add (m_objectBottles[x]);
			}

			foreach (Button btn in m_listOfButtons) 
			{
				m_listOfButtonsToIterate.Add (btn);
			}

			// Reverse for loop for proper stacking
			for (int x = m_objectBottles.Count; x > -1; x--) 
			{
				if (PlayerPrefs.GetInt ("birdbought" + x) > 0) 
				{
					m_listOfButtonsToIterate.RemoveAt (x);
					m_listOfBottlesToIterate.RemoveAt (x);
				}
			}

			// No more items to buy
			if (m_listOfBottlesToIterate.Count < 1) 
			{
				m_objectUnlockButton.gameObject.SetActive (false);
			}
		}

		m_textPrice.text = "" + GameScene.instance.GetCurrentShopPrice () + " COINS";

		/*for (int x = 0; x < 10; x++) {
			if (PlayerPrefs.GetInt ("ballbought" + x) > 0) {
				GameScene.instance.m_listShopBalls [x].objectImage.SetActive (true);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (0, 0, 0, 0.2f);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (false);
			} else {
				GameScene.instance.m_listShopBalls [x].objectImage.SetActive (false);
				GameScene.instance.m_listShopBalls [x].objectImage.GetComponent<Image> ().color = new Color (1,1,1,1);
				GameScene.instance.m_listShopBalls [x].imageButtons.color = m_colorNotboughBalls;
				GameScene.instance.m_listShopBalls [x].objectPrice.SetActive (true);
			}
		}*/

//		int y = 0;
//		foreach (Text txt in m_listText) {
//			if (PlayerPrefs.GetInt ("birdbought" + y) > 0)
//				txt.text = "";
//			else
//				txt.text = GameScene.instance.GetCurrentShopPrice () + " FLIPS";
//
//			y++;
//		}

		//foreach (Text txt in m_listTextBalls) {
		//	txt.text = 250 + "";
		//}

//		if (isNormal) {
//			m_objectUnlockNow.SetActive (false);
//		} else {
//			m_objectUnlockNow.SetActive (true);
//		}
	}

	public override void OnButtonUp(ZButton button)
	{
		if (button.name == "btn_play") 
		{
		}
	}

	public override void OnButtonDown(ZButton button)
	{
		
	}

	public void StartRoullete()
	{
		m_objectUnlockButton.gameObject.SetActive (false);
		m_objectStopButton.gameObject.SetActive (true);
		GameScene.instance.PurchaseItem ();
		Iterate ();
	}

	public void Iterate()
	{
		if (m_listOfBottlesToIterate.Count == 1) 
		{
			HighlightCharacter (0);
			return;
		}

		if (m_listOfButtonsToIterate.Count - 1 < m_currentIndex)
			m_currentIndex = 0;
		
		m_listOfButtonsToIterate [m_currentIndex].image.color = m_colorNotSelected;

		if (TARGET_INDEX == 0) 
		{
			m_speed = 0.1f;
		} 
		else 
		{
			m_iterator++;

			if (m_iterator >= TARGET_INDEX) {
				HighlightCharacter (m_currentIndex);
				return;
			} else if (m_iterator >= TARGET_INDEX * 0.9f) {
				m_speed = 0.7f;
			} else if (m_iterator >= TARGET_INDEX * 0.7f) {
				m_speed = 0.3f;
			} else if (m_iterator >= TARGET_INDEX * 0.6f) {
				m_speed = 0.2f;
			}
		}

		IndexIterator ();


		m_listOfButtonsToIterate [m_currentIndex].image.color = m_colorSelected;

		ZAudioMgr.Instance.PlaySFX (m_audioIterate);

		LeanTween.delayedCall (m_speed, Iterate);
	}

	public void HighlightCharacter(int p_currentIndex)
	{
		TARGET_INDEX = 0;
		m_speed = 0.1f;
		m_iterator = 0;

		m_listOfButtonsToIterate [p_currentIndex].image.color = Color.white;

		LeanTween.cancel (m_listOfBottlesToIterate[p_currentIndex].gameObject);
		LeanTween.scale(m_listOfBottlesToIterate [m_currentIndex].gameObject, m_listOfBottlesToIterate [m_currentIndex].gameObject.transform.localScale + new Vector3(1,1,1), 0.2f).setLoopCount (2).setLoopPingPong ().setEase(LeanTweenType.easeInOutCubic);
		m_listOfBottlesToIterate [p_currentIndex].GetComponent<Renderer> ().material.color = Color.white;
		m_listOfBottlesToIterate [p_currentIndex].GetComponent<ZRotator> ().enabled = true;
				 
		m_particleSuccess.gameObject.SetActive (false);
		m_particleSuccess.gameObject.SetActive (true);

		m_casinoCam.gameObject.SetActive (true);
		m_casinoButtons.gameObject.SetActive (true);

		for (int idx = 0; idx < m_casinoBottles.transform.childCount; idx++) 
		{
			if (m_casinoBottles.transform.GetChild (idx).name == m_listOfBottlesToIterate [m_currentIndex].name) 
			{
				m_itemToPurchase = idx;
				m_casinoBottles.transform.GetChild (idx).gameObject.SetActive (true);
			} 
			else 
			{
				m_casinoBottles.transform.GetChild (idx).gameObject.SetActive (false);
			}
				
		}

		ZAudioMgr.Instance.PlaySFX (m_audioSuccess);
		ZAudioMgr.Instance.PlaySFX (m_audioOvation);
	}

	public void StopRoullete()
	{
		TARGET_INDEX = Random.Range (20, 30);
		m_objectStopButton.gameObject.SetActive (false);
		ZAudioMgr.Instance.PlaySFX (m_audioButton);
	}

	public void Reroll()
	{
		m_casinoCam.gameObject.SetActive (false);
		m_casinoButtons.gameObject.SetActive (false);
		m_objectStopButton.gameObject.SetActive (true);

		m_listOfBottlesToIterate [m_currentIndex].GetComponent<Renderer> ().material.color = Color.black;
		m_listOfBottlesToIterate [m_currentIndex].GetComponent<ZRotator> ().enabled = false;


		Iterate ();
	}

	public void Accept()
	{
		m_casinoCam.gameObject.SetActive (false);
		m_casinoButtons.gameObject.SetActive (false);
		m_objectUnlockButton.gameObject.SetActive (true);
		//GameScene.instance.ConfirmCasinoPurchase (m_itemToPurchase);

		int bottleUnlocked = int.Parse(m_objectBottles [m_currentIndex].name);
		GameScene.instance.UseBird (m_itemToPurchase);

		m_itemToPurchase = -1;

		Back ();
	}

	private void SpeedController()
	{
		if (TARGET_INDEX == 0) 
		{
			m_speed = 0.1f;
		} 
		else 
		{
			m_iterator++;

			if (m_iterator >= TARGET_INDEX) 
			{
				HighlightCharacter (m_currentIndex);
				return;
			} 
			else if (m_iterator >= TARGET_INDEX * 0.8) 
			{
				m_speed = 0.4f;
			} 
			else if (m_iterator >= TARGET_INDEX * 0.9) 
			{
				m_speed = 0.2f;
			}
		}
	}

	private void IndexIterator()
	{
		if (m_currentIndex >= (m_listOfButtonsToIterate.Count - 1)) 
		{
			m_currentIndex = 0;
		} 
		else 
		{
			m_currentIndex++;
		}
	}
}
