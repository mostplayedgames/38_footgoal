﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ZDBMgr : ZSingleton<ZDBMgr> {

	public void SetInt(string name, int value)
	{
		PlayerPrefs.SetInt (name, value);
	}

	public void SetFloat(string name, float value)
	{
		PlayerPrefs.SetFloat (name, value);
	}

	public void SetString(string name, string value)
	{
		PlayerPrefs.SetString (name, value);
	}
}
