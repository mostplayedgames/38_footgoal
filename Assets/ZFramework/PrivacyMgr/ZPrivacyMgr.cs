﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ZPrivacyMgr : MonoBehaviour 
{
	[SerializeField]
	CanvasGroup m_CanvasGroup;

	[SerializeField]
	Button m_OkButton;
	bool m_isFadeOut;

	void Awake()
	{
		m_isFadeOut = false;
		m_OkButton.interactable = false;
		m_CanvasGroup.alpha = 1f;
	}

	public void okToggleChange(Toggle _checkToggle)
	{
		if (m_isFadeOut)
			return;

		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);
		m_OkButton.interactable = _checkToggle.isOn;
	}

	public void buttonCallback(int _getIndex)
	{
		if (m_isFadeOut)
			return;

		ZAudioMgr.Instance.PlaySFX(GameScene.instance.m_audioButton);
		switch (_getIndex)
		{
			case 0: //Ok
				StartCoroutine("FadeOutSequence");
			break;
			case 1: //Privacy Policy
				Application.OpenURL("https://mostplayedgames.co/privacy-policy/");
			break;
		}
	}

	IEnumerator FadeOutSequence()
	{
		PlayerPrefs.SetInt(ZGameMgr.m_PrivacyPref, 1);
		m_isFadeOut = true;

		while (m_CanvasGroup.alpha > 0f)
		{
			yield return new WaitForEndOfFrame();
			m_CanvasGroup.alpha = Mathf.MoveTowards(m_CanvasGroup.alpha, 0f, Time.deltaTime * 2f);
		}

		gameObject.SetActive(false);
	}
}
