﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StageBounds : MonoBehaviour 
{
	public bool isRight = false;

	void LateUpdate()
	{
		Vector3 pos = Vector3.zero;
		pos.x = (isRight ? 1.1f :-0.1f);
		transform.position = Camera.main.ViewportToWorldPoint(pos);
	}
}
