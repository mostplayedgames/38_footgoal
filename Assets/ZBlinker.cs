﻿using UnityEngine;
using System.Collections;

public class ZBlinker : MonoBehaviour {

	public float duration = 1;
	public float alphavalue = 0;

	// Use this for initialization
	void Start () {
		Alpha ();
	}
	
	// Update is called once per frame
	void Alpha () {
		LeanTween.alpha (this.gameObject, alphavalue, duration).setLoopPingPong ();
	}
}
