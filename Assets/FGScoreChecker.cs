﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGScoreChecker : MonoBehaviour {

	public FGGoal m_scriptGoal;
	public GameObject m_objectBall;
	public bool levelMode = true;

	void HideGoal()
	{
		m_scriptGoal.gameObject.SetActive (false);
	}

	void EnableBall()
	{
		m_objectBall.GetComponent<Rigidbody2D> ().isKinematic = false;
		//FGKickMode.instance.Gamescene_Score ();
	}

	void OnTriggerEnter2D(Collider2D coll){
		if (coll.gameObject.tag == "Player") {
			if (FGKickMode.instance.currentMode == 0)
			{
				GameScene.instance.Score(1);
				
				LeanTween.delayedCall(1f, EnableBall);
				LeanTween.delayedCall(0.5f, HideGoal);
				m_objectBall = coll.gameObject;
				coll.GetComponent<Rigidbody2D>().isKinematic = true;
				coll.GetComponent<Rigidbody2D>().velocity = Vector3.zero;
			}
			else FGKickMode.instance.AddScore();




		}
	}
}
