﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGBall : MonoBehaviour 
{
	public AudioSource asrc;
	public AudioClip kickSound;

	// Use this for initialization
	void Awake () 
	{
		if (!asrc) asrc = GetComponent<AudioSource>();
	}

	// Update is called once per frame
	void LateUpdate()
	{
		Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
		pos.x = Mathf.Clamp01(pos.x);
		pos.x = Mathf.Clamp(pos.x, -0.1f, 1.1f);
		transform.position = Camera.main.ViewportToWorldPoint(pos);
	}
	void OnCollisionEnter2D(Collision2D coll) 
	{
		if (coll.gameObject.tag == "Die") {
			this.GetComponent<Rigidbody2D> ().AddForce (new Vector3 (0, 12f, 0));
		}

		PlayBallHit();
	}

	void PlayBallHit()
	{
		asrc.pitch = Random.Range(0.9f, 1.1f);
		asrc.PlayOneShot(kickSound);
	}
}
