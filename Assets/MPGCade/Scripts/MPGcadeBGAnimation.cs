﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPGcadeBGAnimation : MonoBehaviour
{
    [SerializeField] 
    Vector2 m_scrollingSpeed;

    [SerializeField]
    RawImage m_ImageComponent;

    void Update()
    {
        Rect _getRect = m_ImageComponent.uvRect;
        _getRect.x = Mathf.Repeat(_getRect.x + (m_scrollingSpeed.x * Time.deltaTime), 1f);
        _getRect.y = Mathf.Repeat(_getRect.y + (m_scrollingSpeed.y * Time.deltaTime), 1f);

        m_ImageComponent.uvRect = _getRect;        
    }
}