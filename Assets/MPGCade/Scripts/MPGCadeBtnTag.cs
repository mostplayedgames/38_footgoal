﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MPGCadeBtnTag : MonoBehaviour 
{
    [SerializeField]
    Image m_ImageComponent;

    [SerializeField]
    Text m_TextComponent;

    public void Initialize(int p_tagTypes)
    { 
        TagProperties _getTag = ZMPGCade.Instance.TagProperties[p_tagTypes];

        m_ImageComponent.color = _getTag.BGColor;
        m_TextComponent.color = _getTag.TextColor;
        m_TextComponent.text = _getTag.Tag;
    }
}
