﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGGoal : MonoBehaviour 
{
	public Vector2 m_initPos;
	public bool m_isVertical = false;
	public float m_moveDistance = 0;
	public float m_moveSpeed = 0;
	public float m_rotationSpeed = 0;
	public bool m_isReturning = false;

	public void SetGoal(float distance, float speed, float rotSpeed)
	{
		m_initPos = transform.position;
		m_moveDistance = distance;
		if (speed < 0) m_isVertical = true;
		else m_isVertical = false;

		m_moveSpeed = Mathf.Abs(speed);
		m_rotationSpeed = rotSpeed;
		m_isReturning = false;
	}

	private void Update()
	{
		if (m_moveSpeed != 0)
		{
			if (!m_isVertical)
			{
				Vector3 newPos = new Vector3(transform.position.x + m_moveSpeed * Time.deltaTime, transform.position.y);

				if ((newPos.x >= m_initPos.x + m_moveDistance && !m_isReturning) || (newPos.x <= m_initPos.x && m_isReturning))
				{
					if (m_isReturning) newPos.x = m_initPos.x;
					else newPos.x = m_initPos.x + m_moveDistance;

					m_isReturning = !m_isReturning;
					m_moveSpeed *= -1;
				}

				transform.position = newPos;
			}
			else
			{
				Vector3 newPos = new Vector3(transform.position.x, transform.position.y + m_moveSpeed * Time.deltaTime);

				if ((newPos.y >= m_initPos.y + m_moveDistance && !m_isReturning) || (newPos.y <= m_initPos.y && m_isReturning))
				{
					if (m_isReturning) newPos.y = m_initPos.y;
					else newPos.y = m_initPos.y + m_moveDistance;

					m_isReturning = !m_isReturning;
					m_moveSpeed *= -1;
				}

				transform.position = newPos;
			}
		}

		if (m_rotationSpeed != 0) transform.Rotate(Vector3.forward, m_rotationSpeed);
	}

	private void OnDrawGizmosSelected()
	{
		Gizmos.color = Color.red;
		if (m_moveSpeed != 0)
		{
			if (m_isVertical)
			{
				Gizmos.DrawSphere(transform.position + new Vector3(0, m_moveDistance), 1.0f);
			}
			else
			{
				Gizmos.DrawSphere(transform.position + new Vector3(m_moveDistance, 0), 1.0f);
			}
		}
	}
}
