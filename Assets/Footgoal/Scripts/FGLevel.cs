﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FGLevel : MonoBehaviour 
{
	public Transform m_player;
	public Transform m_ball;
	public Transform m_goal;

	public Vector2 m_playerInitPos;
	public Vector2 m_ballInitPos;
	public Vector2 m_goalInitPos;

	public Vector3 m_cameraPos;
	public float m_cameraSize;

	public bool m_useDefaultBall = true;
	public bool m_useDefaultCamera = true;

	public void ResetLevel()
	{
		m_player.position = m_playerInitPos;
		m_ball.position = m_ballInitPos;
		m_goal.position = m_goalInitPos;
	}
}