﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopObject : MonoBehaviour 
{
	public string id;
	public int index;

	public Image shopObjImage;
	public Text shopObjName;
	public Image shopObjBackground;

	public GameObject lockObject;
	public Text price;

	public Button changeCharButton;

	public Color selectedColor;
	public Color deselectedColor;
	public Color lockedColor;

	public void Start()
	{
		UpdatePrice();
	}

	public void UpdatePrice()
	{
//		price.text = GameScene.instance.m_coinsNeeded.ToString();
	}

	public void UpdatePrice(string newPrice)
	{
		if (price) price.text = newPrice;
	}

	public void Unlock()
	{
	
		if (FGKickMode.instance)
		{
			if (FGKickMode.instance.UnlockItem(id))
			{
				UpdateUnlocked();
			}
		}
		
	}

	public void UpdateUnlocked()
	{
		shopObjImage.gameObject.SetActive(true);
		lockObject.SetActive(false);
		changeCharButton.interactable = true;
		shopObjBackground.color = selectedColor;
		// shopObjBackground.color = new Color(1.0f, 0.875f, 0.0f);
	}

	public void UpdateSelected(bool locked, bool selected)
	{
		if (selected)
		{
			shopObjBackground.color = selectedColor; // new Color(1.0f, 0.875f, 0.0f);
		}
		else if (locked)
		{
			shopObjBackground.color = lockedColor; // new Color(0.125f, 0.25f, 0.5f);
		}
		else
		{
			shopObjBackground.color = deselectedColor; // new Color(0f, 0.5f, 1.0f);
		}
	}

	public void ChangeSkin()
	{
		if (FGKickMode.instance) FGKickMode.instance.ChangeCharacter(id);
	}

	public void MakeEntry(string newId, int newIndex, Sprite newImg, bool locked, string name, bool current=false)
	{
		id = newId;
		index = newIndex;
		shopObjImage.sprite = newImg;

		lockObject.SetActive(locked);
		shopObjName.text = name;
		shopObjImage.gameObject.SetActive(!locked);

		if (current)
		{
			shopObjBackground.color = selectedColor;
			changeCharButton.interactable = true;
		}
		else if (locked)
		{
			//			shopObjImage.color = new Color(0f, 0.5f, 1.0f);
			shopObjBackground.color = lockedColor;
			changeCharButton.interactable = false;
		}
		else
		{
			shopObjBackground.color = deselectedColor;// new Color(0f, 0.5f, 1.0f);
			changeCharButton.interactable = true;
		}
	}

}
