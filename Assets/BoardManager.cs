﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardManager : ZSingleton<BoardManager> {

	public GameObject m_objEnterNet;
	public GameObject m_objExitNet;

	public GameObject m_objBoard;
	public GameObject m_objBoardCollider;
	public GameObject m_objShadow;
	public GameObject m_objNet;
	public AudioClip m_audioSwoosh;
	public bool m_isAnimating = false;

	void Update()
	{
		UpdateShadow ();
	}

	public void EnableBoard()
	{
		LeanTween.cancel (this.gameObject);
		m_objEnterNet.gameObject.SetActive (true);
		m_objExitNet.gameObject.SetActive (true);
		ResetNet ();
		IsBoardless (false);
	}

	public void DisableBoard()
	{
		m_objEnterNet.gameObject.SetActive (false);
		m_objExitNet.gameObject.SetActive (false);
	}

	public void ResetNet()
	{
		LeanTween.cancel (m_objNet);
		m_objNet.transform.localScale = new Vector3 (1.18f, 0.53f, 1f);
	}

	public void AnimateNet()
	{
		LeanTween.cancel (m_objNet);
		m_objNet.transform.localScale = new Vector3 (1.18f, 0.53f, 1f);
		LeanTween.scale (m_objNet.gameObject, new Vector3(0.7f, 1.2f, 0.8f), 0.15f).setLoopPingPong ().setLoopCount (2).setEase(LeanTweenType.easeOutSine);
		ZAudioMgr.Instance.PlaySFX (m_audioSwoosh);
		//DisableBoard ();
	}

	public void UpdateTransform(Vector3 p_position)
	{
		this.transform.localPosition = p_position;
	}

	public void UpdateScale(Vector3 p_scale)
	{
		this.transform.localScale = p_scale;
	}

	public void AnimateBoard(string p_direction, float p_animationSpeed)
	{
		LeanTween.cancel (this.gameObject);

		switch(p_direction)
		{
		case "X":
			{
				LeanTween.moveLocalX (this.gameObject, this.transform.localPosition.x - 30, p_animationSpeed).setLoopPingPong ();
			}
			break;

		case "Y":
			{
				LeanTween.moveLocalY (this.gameObject, this.transform.localPosition.x - 70, p_animationSpeed).setLoopPingPong ();
			}
			break;

		case "Z":
			{
				LeanTween.moveLocalY (this.gameObject, this.transform.localPosition.y - 30, p_animationSpeed).setLoopPingPong ();
				LeanTween.moveLocalX (this.gameObject, this.transform.localPosition.x - 40, p_animationSpeed).setLoopPingPong ();
			}
			break;
		}
	}

	public void IsBoardless(bool p_isBoardless)
	{
		if (p_isBoardless) 
		{
			m_objBoard.transform.localEulerAngles = new Vector3 (0, -90, 0);
			m_objBoard.transform.localPosition = new Vector3 (0.15f, -0.03f, 0.05f);
			m_objBoard.GetComponent<Collider2D> ().enabled = false;
			m_objBoardCollider.gameObject.SetActive (false);

			m_objShadow.transform.localPosition = new Vector3 (118, -82, 5.7f);
			m_objShadow.transform.localEulerAngles = new Vector3 (0, 180, 0);
		} 
		else 
		{
			m_objBoard.transform.localEulerAngles = new Vector3 (0, 0, 0);
			m_objBoard.transform.localPosition = new Vector3 (0, -0.03f, -0.12f);
			m_objBoard.GetComponent<Collider2D> ().enabled = true;
			m_objBoardCollider.gameObject.SetActive (true);
			m_objShadow.transform.localPosition = new Vector3 (123, -82, 0.8f);
			m_objShadow.transform.localEulerAngles = new Vector3 (0, 270, 0);
		}
	}

	private void ResetBoardState()
	{
		m_isAnimating = false;
	}

	private void UpdateShadow()
	{
		//Debug.Log ("Shadows Are Updating");
		m_objShadow.transform.localPosition = new Vector3 (this.transform.position.x, m_objShadow.transform.position.y, m_objShadow.transform.localPosition.z);
	}
}
